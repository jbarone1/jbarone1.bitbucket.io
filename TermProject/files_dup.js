var files_dup =
[
    [ "DRV8847.py", "DRV8847_8py.html", [
      [ "DRV8847.DRV8847", "classDRV8847_1_1DRV8847.html", "classDRV8847_1_1DRV8847" ],
      [ "DRV8847.Motor", "classDRV8847_1_1Motor.html", "classDRV8847_1_1Motor" ]
    ] ],
    [ "imu.py", "imu_8py.html", "imu_8py" ],
    [ "main.py", "main_8py.html", "main_8py" ],
    [ "shares.py", "shares_8py.html", [
      [ "shares.Share", "classshares_1_1Share.html", "classshares_1_1Share" ],
      [ "shares.Queue", "classshares_1_1Queue.html", "classshares_1_1Queue" ]
    ] ],
    [ "task_controller.py", "task__controller_8py.html", "task__controller_8py" ],
    [ "task_imu.py", "task__imu_8py.html", "task__imu_8py" ],
    [ "task_motor.py", "task__motor_8py.html", [
      [ "task_motor.Task_Motor", "classtask__motor_1_1Task__Motor.html", "classtask__motor_1_1Task__Motor" ]
    ] ],
    [ "task_TP.py", "task__TP_8py.html", "task__TP_8py" ],
    [ "task_user.py", "task__user_8py.html", "task__user_8py" ],
    [ "touchpanel.py", "touchpanel_8py.html", [
      [ "touchpanel.TouchPanel", "classtouchpanel_1_1TouchPanel.html", "classtouchpanel_1_1TouchPanel" ]
    ] ]
];